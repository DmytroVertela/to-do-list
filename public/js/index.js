function sendData(url, method, data, cb) {
    $.ajax({
      url: url,
      method: method,
      data: data,
      contentType: 'application/json',
      success: function(data) {
        cb(data);
      }
    });
  }

  var li = function(_TeMa) {
    // return `<tr data-rowid='${user.id}'><td>${user.id}</td><td>${user.name}</td> <td>${user.age}</td><td><a class='editLink' data-id='${user.id}'>Изменить</a> | <a class='removeLink' data-id='${user.id}'>Удалить</a></td></tr>`;
    // console.log(_TeMa.target);
    if(_TeMa.done){
      return `<li class="checked" slot="${_TeMa.id}">${_TeMa.target}<span class="close" onclick="rm_rf(this)">×</span></li>`;
    }else{
      return `<li slot="${_TeMa.id}">${_TeMa.target}<span id="close" class="close" onclick="rm_rf(this)">×</span></li>`;
    }
  };
  
function _4eKak() {
    sendData('/to/do', 'GET', {}, todo => {
      let lis = '';
      $.each(todo, (index, todo) => {
        lis += li(Object.assign(todo, {id:index}))
      });
    //   console.log(lis);
      $('#myUL').append(lis);
    });
  }

function HaMyTuTb() {
    var inputValue = document.getElementById("myInput").value;
    if (inputValue === '') {
      alert("You must write something!");
    } else {
        $('#myInput').submit();
    //   console.log('submited');
    }
    document.getElementById("myInput").value = "";
}

$('#myInput').on('submit', function(e){
    e.preventDefault();
    let target = this.value;
    let done = false;
    sendData('/to/do', 'POST', JSON.stringify({target, done}), todo => {
        $('#myUL').append(li(Object.assign(todo)));
    });
});

var list = document.querySelector('ul');
list.addEventListener('click',function(ev){
    if(ev.target.tagName === "LI"){
        ev.target.classList.toggle('checked');
        var id = ev.target.slot;
        var done;
        var target = ev.target.textContent;
        if(ev.target.className === 'checked'){
            done = true;
        }else{
            done = false;
        }
        sendData('to/do/'+id, 'PUT', JSON.stringify({target, done}), todo => {});
    }
}, false);

function rm_rf(e){
    var div = e.parentElement;
    var id = div.slot;
    // console.log(div.slot);
    sendData('/to/do/'+id, 'DELETE', {}, todo => {
        div.remove();
    }); 
}

_4eKak();